# @atlaskit/popper

## 0.1.1
- [patch] Updated dependencies [acd86a1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/acd86a1)
  - @atlaskit/button@9.0.4
  - @atlaskit/theme@5.1.2
  - @atlaskit/docs@5.0.2

## 0.1.0
- [minor] Dev release for popper [e987222](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e987222)
