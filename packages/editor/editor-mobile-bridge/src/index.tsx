import 'babel-polyfill';
import * as ReactDOM from 'react-dom';
import mobileEditor from './mobile-editor-element';

ReactDOM.render(mobileEditor(), document.getElementById('editor'));
