export {
  default as GlobalQuickSearch,
} from './components/GlobalQuickSearchWrapper';

export { Config } from './api/configureSearchClients';

export {
  default as withFeedbackButton,
} from './components/feedback/withFeedbackButton';
